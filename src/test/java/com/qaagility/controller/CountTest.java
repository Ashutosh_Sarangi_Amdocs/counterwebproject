package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CountTest {
    @Test
    public void divideWith0() throws Exception {

        int k= new Count().divide(5,0);
        assertEquals("Result", Integer.MAX_VALUE, k);
        
    }
   @Test
    public void divideWithNon0() throws Exception {

        int k= new Count().divide(8,2);
        assertEquals("Result", 4, k);

    }
}
